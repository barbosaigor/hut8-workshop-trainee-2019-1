// Navbar
const docNavbar = document.getElementsByTagName('nav')[0];
const navbarState = {
    active: false
};

// Toggle nav HTML tag class either active or not
window.addEventListener('scroll', () => {
    if (window.scrollY >= 40 && !navbarState.active) {
        navbarState.active = true;
        docNavbar.classList.toggle('active');
    }
    else if(window.scrollY < 40 && navbarState.active) {
        navbarState.active = false;
        docNavbar.classList.toggle('active');
    }
});

// Form trigger
const docForm = document.getElementsByTagName('form')[0];

docForm.addEventListener('submit', event => {
    event.preventDefault();
    msg = `Olá ${docForm.name.value}, já iremos te responder no email ${docForm.email.value} sobre ${docForm.subject.value}`;
    alert(msg);
    docForm.reset();
});